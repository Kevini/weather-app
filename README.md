
### Weather App ###

This is a simple web application built in [Laravel](https://laravel.com/) and [Tailwindcss](https://tailwindcss.com/) framework with the Open Weather API & Algolia Cities showing:

* Weather for different Cities

This build was off a Dribble Weather UI

### ScreenShot ###

![Screenshot](public/images/screen-shot.png)

### How do I get set up? ###

* Clone the repo and cd into it
* composer install
*  npm install
*  npm run dev
* Set your Open Weather Key in your .env file.
* php artisan key:generate
* php artisan serve
* Visit localhost:8000 in your browser

### Contact ###
Feel free to reach out to me on [Mail](mailto:kevinci@live.com.com?subject=[BitBucket-Weather-App]) for any other questions or comments on this project.

